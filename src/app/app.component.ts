import { Component } from '@angular/core';
import { GameService, GameChoice, GameStage } from './game.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Hack to make enums available in template
  GameStage: typeof GameStage = GameStage;
  GameChoice: typeof GameChoice = GameChoice;

  constructor(public game: GameService) {}
}
